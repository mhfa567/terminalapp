
package terminal;

import java.text.SimpleDateFormat;
import java.util.Date;
public class Bus 
{
    private String name;
    private String destination;
    private String day;
    public  byte hour;
    private byte numberChair;
    private Driver driver;
    
    public Bus(String name,String destination,String day,byte hour,byte numberChair,Driver driver)
    {
        this.name=name;
        this.destination=destination;
        this.day=day;
        this.hour=hour;
        this.numberChair=numberChair;
        this.driver=driver;
    }
    public byte getDayByte()
    {
        byte dayByte=0;
        if (day.equals("monday")) 
        {
            dayByte=1;
        }
        else if (day.equals("tuesday")) 
        {
            dayByte=2;
        }
         else if (day.equals("wednesday")) 
        {
            dayByte=3;
        }
         else if (day.equals("thursday")) 
        {
            dayByte=4;
        }
        else if (day.equals("friday")) 
        {
            dayByte=5;
        }
        else if (day.equals("saturday")) 
        {
            dayByte=6;
        }             
        return dayByte;
    }

    @Override
    public String toString() {
        return  name + " " +destination+ " " + day+" "+numberChair+ " " + hour + ":" + driver;
    }

    public void setNumberChair(byte numberChair) {
        this.numberChair = numberChair;
    }

    public byte getHour() {
        return hour;
    }

    public String getDay() {
        return day;
    }

    public String getDestination() {
        return destination;
    }

    public String getName() {
        return name;
    }

    public byte getNumberChair() {
        return numberChair;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    
    
    
}
