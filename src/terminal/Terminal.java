
package terminal;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.print.DocFlavor;

public class Terminal 
{
    
    public static void main (String[] args)throws Exception
    {
        Scanner scanner=new Scanner(System.in);
        Date date = new Date(); 
        
        ArrayList<Driver> arrayListDriver=new ArrayList<Driver>();
        File fileDriver=new File("Driver.txt");
        FileWriter fileWriterDriver=new FileWriter(fileDriver,true);
        Scanner fileScannerDriver=new Scanner(fileDriver);
        findDriver(fileScannerDriver, arrayListDriver);
        
        ArrayList<Bus> arrayListBus=new ArrayList<Bus>();
        File fileBus=new File("Bus.txt");
        FileWriter fileWriterBus=new FileWriter(fileBus,true);
        Scanner fileScannerBus=new Scanner(fileBus);
        findBus(fileScannerBus, arrayListBus);
        
        System.out.println("Hello");
        help();
        
        while(true)
        {
            String input=scanner.nextLine();
            if (input.equalsIgnoreCase("List Driver")|| input.equalsIgnoreCase("ListDriver")) 
            {
                showListDriver(arrayListDriver);
            }
            else if (input.equalsIgnoreCase("Add Bus")|| input.equalsIgnoreCase("AddBus"))
            {
                addBus(arrayListBus,fileBus);
            }
            else if (input.equalsIgnoreCase("Add passenger")|| input.equalsIgnoreCase("Addpassenger"))
            {
                System.out.println("List Bus :");
                updateBusLlst(arrayListBus, date, fileBus);
                System.out.println("Choose a bus:");
                String nameBus=scanner.nextLine();
                chooseBus(nameBus, arrayListBus,fileBus);
            }
            else if(input.equalsIgnoreCase("exit"))
            {
                break;
            }  
            else
            {
                help();
            }
        }
    }
     private static void help() 
    {
        System.out.println("List Driver:");
        System.out.println("Add Passenger:");
        System.out.println("Add Bus:");
        System.out.println("Exit:");
        System.out.println("Help:");
    }
    private static void findDriver(Scanner fileScannerDriver, ArrayList<Driver> arrayListDriver)
    {
        while (fileScannerDriver.hasNext())
        {
            String line=fileScannerDriver.nextLine();
            String [] lineSplited=line.split(":");
            String name=lineSplited[0];
            int account=Integer.parseInt(lineSplited[1]);
            Driver driver =new Driver(name, account);
            arrayListDriver.add(driver);
        }
    }
    private static void showListDriver(ArrayList<Driver> arrayListDriver)
    {
        for (Driver driver : arrayListDriver) 
        {
            System.out.println(driver);
        }
    }
    private static void findBus(Scanner fileScannerBus, ArrayList<Bus> arrayListBus)  {
        while (fileScannerBus.hasNext())
        {
            String line=fileScannerBus.nextLine();
            String [] lineSplited=line.split(":");
            String name=lineSplited[0];
            String destination=lineSplited[1];
            String day=lineSplited[2];
            byte numberChair=Byte.parseByte(lineSplited[3]);
            byte hour=Byte.parseByte(lineSplited[4]);
            String nameDriver=lineSplited[5];
            int account=Integer.parseInt(lineSplited[6]);
            Driver driver=new Driver(nameDriver, account);
            Bus bus=new Bus(name, destination,day,hour, numberChair,driver);
            arrayListBus.add(bus);
        }
    }
    private static void addInFileBus(File fileBus, ArrayList<Bus> arrayListBus) {
        try
        {
            FileWriter fileWriterBus=new FileWriter(fileBus);
            BufferedWriter bufferedWriter=new BufferedWriter(fileWriterBus);
            
            for (int i = 0; i <arrayListBus.size() ; i++)
            {
                bufferedWriter.write(arrayListBus.get(i).getName()+ ":" +arrayListBus.get(i).getDestination()+":"+arrayListBus.get(i).getDay()+":"+ arrayListBus.get(i).getHour()+":"+arrayListBus.get(i).getNumberChair()+":"+arrayListBus.get(i).getDriver().getName()+":"+arrayListBus.get(i).getDriver().getAccount());
                bufferedWriter.newLine();
            }
            bufferedWriter.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
    }
    private static void updateBusLlst(ArrayList<Bus> arrayListBus, Date date,File fileBus) {
        for (Bus bus : arrayListBus)
        {
            if(bus.getDayByte()<date.getDay())
            {
                bus.setNumberChair((byte)30);
            }
            else if(bus.getDayByte()==date.getDay())
            {
                if (bus.getHour()<=date.getHours())
                {
                    bus.setNumberChair((byte)30);
                }
            }
            System.out.println(bus);
        }
         addInFileBus(fileBus, arrayListBus);
    }
    private static void chooseBus(String nameBus,ArrayList<Bus> arrayListBus,File fileBus)
    {
        for(Bus bus :arrayListBus ) 
        {
            if(bus.getName().equalsIgnoreCase(nameBus)) 
            {
                if (bus.getNumberChair()>0) 
                {
                    byte busChair=(byte)(bus.getNumberChair()-1);
                    Driver driver=bus.getDriver();
                    int account=driver.getAccount()+100;
                    driver.setAccount(account);
                    bus.setNumberChair(busChair);
                    bus.setDriver(driver);
                    addInFileBus(fileBus, arrayListBus);
                    return;
                }
                else 
                {
                    System.out.println("The bus seats are full!!");
                    return;
                }
            }
        }
        System.out.println("We do not have such a bus!!");
    }
    private static  void addBus(ArrayList<Bus> arrayListBus,File fileBus)
    {
        Scanner scanner=new Scanner(System.in);
        System.out.println("add name Bus");
        String name=scanner.nextLine();
        System.out.println("add destination");
        String destination=scanner.nextLine();
        System.out.println("add day");
        String day=scanner.nextLine();
        System.out.println("add hour");
        byte hour=scanner.nextByte();
        System.out.println("add number of chair");
        byte numberChair=scanner.nextByte();
        System.out.println("add name driver");
        String nameDriver=scanner.nextLine();
        System.out.println("add account driver");
        int account=scanner.nextInt();
        Driver driver=new Driver(nameDriver,account);
        Bus bus=new Bus(name,destination,day,hour,numberChair,driver);
        arrayListBus.add(bus);
        addInFileBus(fileBus,arrayListBus);
    }
}
