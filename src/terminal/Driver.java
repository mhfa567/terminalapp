
package terminal;

public class Driver 
{
    private String name;
    private int account;
    public Driver(String name,int account)
    {
        this.name=name;
        this.account=account;
    }

    public String getName() 
    {
        return name;
    }

    public int getAccount() 
    {
        return account;
    }

    public void setAccount(int account) 
    {
        this.account = account;
    }

    @Override
    public String toString() 
    {
        return name + " " + ": " + account;
    }
    
}
